package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;

public class MenuFactoryImpl implements MenuFactory{

    private static final String INUZUMA = "InuzumaRamen";
    private static final String LIYUAN = "LiyuanSoba";
    private static final String MONDO = "MondoUdon";
    private static final String SNEVNEZHA = "SnevnezhaShirataki";

    @Override
    public Menu getMenu(String name, String menuName) {
        switch (menuName) {
            case INUZUMA:
                return new InuzumaRamen(name);
            case LIYUAN:
                return new LiyuanSoba(name);
            case MONDO:
                return new MondoUdon(name);
            case SNEVNEZHA:
                return new SnevnezhaShirataki(name);
            default:
                return null;
        }
    }
}
