package id.ac.ui.cs.advprog.tutorial4.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MenuInitializer {

    @Autowired
    private MenuRepository repo;


    @PostConstruct
    public void init() {
        MenuFactory factory = repo.getMenuFactory();
        repo.add(factory.getMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba"));
        repo.add(factory.getMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen"));
        repo.add(factory.getMenu("Good Hunter Cheese Chicken Udon", "MondoUdon"));
        repo.add(factory.getMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki"));
    }

}
