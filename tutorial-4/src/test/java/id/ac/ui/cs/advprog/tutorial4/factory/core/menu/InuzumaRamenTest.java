package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class InuzumaRamenTest {

    private InuzumaRamen ramen = new InuzumaRamen("Ayaka");

    //Ingridients:
    //Noodle: Ramen
    //Meat: Pork
    //Topping: Boiled Egg
    //Flavor: Spicy

    @Test
    public void testInuzumaRamenUseRamenNoodle() {
        Noodle noodle = ramen.getNoodle();
        assertTrue(noodle instanceof Ramen);
    }

    @Test
    public void testInuzumaRamenUsePorkMeat() {
        Meat meat = ramen.getMeat();
        assertTrue(meat instanceof Pork);
    }

    @Test
    public void testInuzumaRamenUseBoiledEggTopping() {
        Topping topping = ramen.getTopping();
        assertTrue(topping instanceof BoiledEgg);
    }

    @Test
    public void testInuzumaRamenUseSpicyFlavor() {
        Flavor flavor = ramen.getFlavor();
        assertTrue(flavor instanceof Spicy);
    }

}
