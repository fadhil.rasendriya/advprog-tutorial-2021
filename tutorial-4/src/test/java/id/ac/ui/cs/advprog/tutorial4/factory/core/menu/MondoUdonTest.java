package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonTest {
    private MondoUdon udon = new MondoUdon("Groza");

    //Ingridients:
    //Noodle: Udon
    //Meat: Chicken
    //Topping: Cheese
    //Flavor: Salty

    @Test
    public void testMondoUdonUseUdonNoodle() {
        Noodle noodle = udon.getNoodle();
        assertTrue(noodle instanceof Udon);
    }

    @Test
    public void testMondoUdonUseChickenMeat() {
        Meat meat = udon.getMeat();
        assertTrue(meat instanceof Chicken);
    }

    @Test
    public void testMondoUdonUseCheeseTopping() {
        Topping topping = udon.getTopping();
        assertTrue(topping instanceof Cheese);
    }

    @Test
    public void testMondoUdonUseSaltyFlavor() {
        Flavor flavor = udon.getFlavor();
        assertTrue(flavor instanceof Salty);
    }
}
