package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiTest {

    private SnevnezhaShirataki shirataki = new SnevnezhaShirataki("Anna");

    //Ingridients:
    //Noodle: Shirataki
    //Meat: Fish
    //Topping: Flower
    //Flavor: Umami

    @Test
    public void testSnevnezhaShiratakiUseShiratakiNoodle() {
        Noodle noodle = shirataki.getNoodle();
        assertTrue(noodle instanceof Shirataki);
    }

    @Test
    public void testSnevnezhaShiratakiUseFishMeat() {
        Meat meat = shirataki.getMeat();
        assertTrue(meat instanceof Fish);
    }

    @Test
    public void testSnevnezhaShiratakiUseFlowerTopping() {
        Topping topping = shirataki.getTopping();
        assertTrue(topping instanceof Flower);
    }

    @Test
    public void testSnevnezhaShiratakiUseUmamiFlavor() {
        Flavor flavor = shirataki.getFlavor();
        assertTrue(flavor instanceof Umami);
    }
}
