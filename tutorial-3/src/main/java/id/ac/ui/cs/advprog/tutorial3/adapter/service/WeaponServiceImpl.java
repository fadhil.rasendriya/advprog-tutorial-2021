package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired SpellbookRepository spellbookRepository;

    @Override
    public List<Weapon> findAll() {

        for (Bow bow:
             bowRepository.findAll()) {
            Weapon bowWeapon = weaponRepository.findByAlias(bow.getHolderName());
            if(bowWeapon == null) {
                weaponRepository.save(new BowAdapter(bow));
            }
        }
        for (Spellbook spellbook:
             spellbookRepository.findAll()) {
            Weapon spellbookWeapon = weaponRepository.findByAlias(spellbook.getHolderName());
            if(spellbookWeapon == null) {
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }
        }

        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if(attackType == 0) {
            logRepository.addLog(String.format("%s attacked with %s (normal attack): %s", weapon.getHolderName(), weapon.getName(), weapon.normalAttack()));
        } else {
            logRepository.addLog(String.format("%s attacked with %s (charged attack): %s", weapon.getHolderName(), weapon.getName(), weapon.chargedAttack()));
        }
        weaponRepository.save(weapon);

    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
