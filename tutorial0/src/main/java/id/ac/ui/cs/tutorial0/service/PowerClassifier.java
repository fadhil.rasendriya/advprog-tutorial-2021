package id.ac.ui.cs.tutorial0.service;

public interface PowerClassifier {
    public String classifyPower(int power);
}
